/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** GameSnake
*/

#ifndef GAMESNAKE_HPP_
#define GAMESNAKE_HPP_
#include "core.hpp"
#include "Parse.hpp"

namespace game
{
    typedef struct CorpSnake CorpSnake;
    struct CorpSnake
    {
	    int line;
	    int column;
    };
    class GameSnake : public Arcade::Parse
    {
        public:
            GameSnake();
            void read_highscore();
            void write_highscore();
            std::vector<std::string> beginSnake(std::vector<std::string>, int);
            void Left(std::vector<std::string>);
            void endOfGame(std::vector<std::string>);
            std::vector<std::string> replaceApple(std::vector<std::string>);
            void up(std::vector<std::string>);
            std::vector<std::string> findBody(std::vector<std::string> Map);
            ~GameSnake();
            int touchePress;
            int score = 0;
            int highscore;
            int Exit;
        protected:
            std::vector<CorpSnake> tabCorp;
            int checkround;
            void sleep(float seconds);
            int Corp1X;
            int Corp1Y;
            int z = 0;
            int corp = 0;
            int posX;
            int posY;
            int posY_tail;
            int posX_tail;
        private:
    };
}

#endif /* !GAMESNAKE_HPP_ */

/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** core
*/

#ifndef CORE_HPP_
#define CORE_HPP_
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <unistd.h>
#include <ncurses.h>
#include "SDL_Map.hpp"
#include "ncurses_snake.hpp"

class core
{
private:
    /* data */
public:
    core();
    core(int i);
    void game_over_snake(int i);
    void game_over_Pacman(double i);
    virtual void game_win_snake();
    void chooseLib(int);
    void chooseGame(int);
    int go_to_sdl();
    int go_to_sfml();
    int go_to_sdl_snake();
    int go_to_ncurses();
    ~core(void);
protected:
    std::vector<std::string> Map;
    std::vector<std::string> MapPacman;
    int touche;
    int checkLib;
    int checkGame;
};

template <typename score>
score Score(const score& nb)
{
    return (nb);
}

#endif /* !CORE_HPP_ */

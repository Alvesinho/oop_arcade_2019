/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** ncurses_snake
*/

#ifndef NCURSES_SNAKE_HPP_
#define NCURSES_SNAKE_HPP_

#include <ncurses.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <curses.h>
#include <vector>
#include "ncurses_menu.hpp"
#include "core.hpp"

class snake_ncurses 
{
    public:
        int map_ncurses(std::vector<std::string>);
        int game_loop(std::vector<std::string> Map);
        void ncurses_game_over();
        void ncurses_game_win();
        int game;
        int lib;
        int Exit = 0;
    protected:
        
    private:
        WINDOW *snake_win, *snake_game_over, *snake_game_win;
        const char *map_center = "*";

};

#endif /* !NCURSES_SNAKE_HPP_ */

/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_Mur
*/

#ifndef SDL_MUR_HPP_
#define SDL_MUR_HPP_
#include "SDL_Map.hpp"

class SDL_Mur : public SDL_map::SDL_Map
{
    public:
        SDL_Mur();
        void create_Mur(std::vector<SDL_Surface*> tabImage[1000][10000]);
        ~SDL_Mur();

    protected:
    private:
};

#endif /* !SDL_MUR_HPP_ */

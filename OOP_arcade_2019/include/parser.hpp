/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** parser
*/

#ifndef PARSER_HPP_
#define PARSER_HPP_

#include <string>
#include <iostream>
#include <fstream>

class parser
{
    private:
    public:
        parser();
        ~parser();
        int errors_gestions(int ac); 
        int read_file(int ac, char **av);
        std::string recup_value(std::string str);
        void *sym_lib(void *handle, std::string lib);
        void *close_lib(void *handle);
        void *load_lib(std::string path);
        int values[100];
        int lib;
        int game;

};

#endif

/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL
*/

#include "../../include/SDL.hpp"
#include "../../include/SDL_MenuSnak.hpp"
#include "../../include/core.hpp"
#include "../../include/SDL_MenuPacMan.hpp"

SDL::SDL()
{
}

int SDL::goToMenuSnak()
{
    SDL_MenuSnak p;

    p.init_background();
    p.init_image_snake();
    p.init_position();
    p.blit_to_surface_snake();
    p.flip_ecran_snake();
    this->stockNb = p.pause_snake();
    return (this->stockNb);
}

int SDL::goToMenuPacMan()
{
    SDL_MenuPacMan pac;

    pac.init_background();
    pac.init_image_Pacman();
    pac.init_position();
    pac.blit_to_surface_PacMan();
    pac.flip_ecran_PacMan();
    this->stockNb = pac.pause_PacMan();
    return (this->stockNb);
}

int SDL::changeToMouse()
{
    if (SDL::event.motion.x > 55 && SDL::event.motion.x < 175
        && SDL::event.motion.y > 130 && SDL::event.motion.y < 190) {
             if (event.button.button == SDL_BUTTON_LEFT)
                 this->stockNb = goToMenuSnak();
        SDL_BlitSurface(SDL::snackBlanc, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);
        }
    else if (SDL::event.motion.x > 530 && SDL::event.motion.x < 635
        && SDL::event.motion.y > 130 && SDL::event.motion.y < 190) {
            if (event.button.button == SDL_BUTTON_LEFT)
                 this->stockNb = goToMenuPacMan();
            SDL_BlitSurface(SDL::PacmanBlanc, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);
        }
    else if (SDL::event.motion.x > 295 && SDL::event.motion.x < 410
        && SDL::event.motion.y > 385 && SDL::event.motion.y < 450) {
            SDL_BlitSurface(SDL::ExitBlanc, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);
        if (event.button.button == SDL_BUTTON_LEFT) {
                 freeSpace();
                 SDL_Quit();
                 exit(EXIT_SUCCESS);
            }
        }
    else if (SDL::event.motion.x > 230 && SDL::event.motion.x < 475
        && SDL::event.motion.y > 208 && SDL::event.motion.y < 267) {
            SDL_BlitSurface(SDL::HsBlanc, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);
        }
    else {
        SDL_BlitSurface(SDL::imageFond, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);  
    }
    return (this->stockNb);
}

void SDL::init_background()
{
    SDL::ecran = SDL_SetVideoMode(720, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("Menu de la lib SDL", NULL);
}

void SDL::init_position()
{
    SDL::positionFond.x = 0;
    SDL::positionFond.y = 0;
}

void SDL::blit_to_surface()
{
    SDL_BlitSurface(SDL::imageFond, NULL, SDL::ecran, &positionFond);
}

void SDL::init_image()
{
    SDL::imageFond = SDL_LoadBMP("lib_graph/sdl1/img/MenuSnake.bmp");
    SDL::snackBlanc = SDL_LoadBMP("lib_graph/sdl1/img/MenuSnakeSnack.bmp");
    SDL::ExitBlanc = SDL_LoadBMP("lib_graph/sdl1/img/MenuSnakeExit.bmp");
    SDL::HsBlanc = SDL_LoadBMP("lib_graph/sdl1/img/MenuSnakeHS.bmp");
    SDL::PacmanBlanc = SDL_LoadBMP("lib_graph/sdl1/img/MenuSnakePacMan.bmp");
}

void SDL::flip_ecran()
{
    SDL_Flip(SDL::ecran);
}

int SDL::pause()
{
    int continuer = 1;

    while (continuer) {
        this->stockNb = changeToMouse();
        SDL_PollEvent(&event);
        switch(SDL::event.type)
        {
            case SDL_QUIT:
                freeSpace();
                 SDL_Quit();
                 exit(EXIT_SUCCESS);
                break;
            case SDL_MOUSEMOTION:
                break;
            case SDL_MOUSEBUTTONUP:
                break;
            case SDL_KEYDOWN:
            switch (SDL::event.key.keysym.sym)
            {
                case SDLK_n:
                    this->stockNb = 1;
                    continuer = 0;
                    break;
                case SDLK_p:
                    this->stockNb = 2;
                    continuer = 0;
                    break;
                default:
                        SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                        break;
            }
            break;
            default:
                    SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                    break;
        break;
        }
        if (this->stockNb == 1 || this->stockNb == 2 || this->stockNb == 4 
        || this->stockNb == 8)
            break;
    }
    return (this->stockNb);
}

void SDL::freeSpace()
{
    SDL_FreeSurface(SDL::imageFond);
    SDL_FreeSurface(SDL::snackBlanc);
    SDL_FreeSurface(SDL::PacmanBlanc);
    SDL_FreeSurface(SDL::ExitBlanc);
    SDL_FreeSurface(SDL::HsBlanc);
}

SDL::~SDL()
{
}

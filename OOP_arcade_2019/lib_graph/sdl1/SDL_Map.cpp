/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_Map
*/

#include "../../include/SDL_Map.hpp"
#include "../../include/SDL_Mur.hpp"
#include "../../include/core.hpp"

SDL_map::SDL_Map::SDL_Map()
{
}

void SDL_map::SDL_Map::init_background_map()
{
    SDL::ecran = SDL_SetVideoMode(720, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("Jeux Snake", NULL);
}

void SDL_map::SDL_Map::init_image_map()
{
    SDL::imageFond = SDL_LoadBMP("lib_graph/sdl1/img/fondMap.bmp");
}

void SDL_map::SDL_Map::init_position_map()
{
    SDL::positionFond.x = 0;
    SDL::positionFond.y = 0;
}

void SDL_map::SDL_Map::flip_ecran_map()
{
    SDL_Flip(SDL::ecran);
}

void SDL_map::SDL_Map::blit_to_surface_map()
{
    SDL_BlitSurface(SDL::imageFond, NULL, SDL::ecran, &positionFond);
}

void SDL_map::SDL_Map::stock_line_and_column(std::vector<std::string> Map)
{
    this->line = Map.size();
    this->Column = Map[0].size() + 1;
}

int SDL_map::SDL_Map::SDL_drawMap(std::vector<std::string> Map)
{
    stock_line_and_column(Map);
    init_background_map();
    init_image_map();
    init_position_map();
    blit_to_surface_map();
    Map = putWall(Map);
    flip_ecran_map();
    pauseMap();
    freeSpaceMap();
    return (this->touche);
}

void SDL_map::SDL_Map::freeSpaceMap()
{
    SDL_FreeSurface(SDL_map::SDL_Map::imageMur);
    SDL_FreeSurface(SDL_Map::SDL_Map::imageSnakehead);
    SDL_FreeSurface(SDL_Map::SDL_Map::imageSnakeCorp);
    SDL_FreeSurface(SDL_map::SDL_Map::imagePomme);
    SDL_FreeSurface(SDL_map::SDL_Map::imagePacGomme);
    SDL_FreeSurface(SDL_map::SDL_Map::imagePacMan);
    SDL_FreeSurface(SDL_map::SDL_Map::imageGhost5);
    SDL_FreeSurface(SDL_map::SDL_Map::imageGhost4);
    SDL_FreeSurface(SDL_map::SDL_Map::imageGhost3);
    SDL_FreeSurface(SDL_map::SDL_Map::imageGhost2);
    SDL_FreeSurface(SDL_map::SDL_Map::imageGhost1);
    SDL_FreeSurface(SDL_map::SDL_Map::imagePacBoule);
    SDL_FreeSurface(SDL::imageFond);
}

void SDL_map::SDL_Map::pauseMap()
{
        SDL_PollEvent(&event);
        //sleep(0.1);
        switch(SDL::event.type)
        {
            case SDL_QUIT:
                freeSpaceMap();
                 SDL_Quit();
                 exit(EXIT_SUCCESS);
                break;
            case SDL_MOUSEMOTION:
                break;
            case SDL_MOUSEBUTTONUP:
                break;
            case SDL_KEYUP:
            switch (SDL::event.key.keysym.sym)
            {
                case SDLK_p:
                    this->touche = 6;
                    break;
                case SDLK_n:
                    this->touche = 5;
                    break;
                case SDLK_UP:
                    this->touche = 2;
                    break;
                case SDLK_RIGHT:
                    this->touche = 3;
                    break;
                case SDLK_LEFT:
                    this->touche = 1;
                    break;
                case SDLK_DOWN:
                    this->touche = 4;
                    break;
                case SDLK_w:
                    this->touche = 9;
                    break;
                case SDLK_x:
                    this->touche = 10;
                    break;
                case SDLK_q:
                    freeSpaceMap();
                    SDL_Quit();
                    exit(EXIT_SUCCESS);
                default:
                        SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                        break;
            }
            break;
            default:
                    SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                    break;
        break;
        }

}

void SDL_map::SDL_Map::loadSprite()
{
    this->imageMur = SDL_LoadBMP("lib_graph/sdl1/img/murs.bmp");
    this->imageSnakehead = SDL_LoadBMP("lib_graph/sdl1/img/tetesnake.bmp");
    this->imageSnakeCorp = SDL_LoadBMP("lib_graph/sdl1/img/SnakeCorp.bmp");
    this->imagePomme = SDL_LoadBMP("lib_graph/sdl1/img/Pomme.bmp");
    this->imagePacGomme = SDL_LoadBMP("lib_graph/sdl1/img/PacGomme.bmp");
    this->imagePacMan = SDL_LoadBMP("lib_graph/sdl1/img/PacMan.bmp");
    this->imageGhost1 = SDL_LoadBMP("lib_graph/sdl1/img/gosht1.bmp");
    this->imageGhost2 = SDL_LoadBMP("lib_graph/sdl1/img/gosht2.bmp");
    this->imageGhost3 = SDL_LoadBMP("lib_graph/sdl1/img/gost3.bmp");
    this->imageGhost4 = SDL_LoadBMP("lib_graph/sdl1/img/gost4.bmp");
    this->imageGhost5 = SDL_LoadBMP("lib_graph/sdl1/img/gost5.bmp");
    this->imagePacBoule = SDL_LoadBMP("lib_graph/sdl1/img/PacBoule.bmp");
}

std::vector<std::string> SDL_map::SDL_Map::putWall(std::vector<std::string> Map)
{
    int pposX = 50;
    int posY = 150;

    loadSprite();
    for (int i = 0; i < this->line; i++, posY += 14) {
        for (int j = 0; j < this->Column; j++,pposX += 14) {
            if (Map[i][j] == '/') {
                this->positionMur.x = j + pposX;
                this->positionMur.y = i + posY;
                SDL_BlitSurface(this->imageMur, NULL, SDL::ecran, &positionMur);
            }
            if (Map[i][j] == 'O') {
                this->positionSnakeHead.x = j + pposX;
                this->positionSnakeHead.y = i + posY;
                SDL_BlitSurface(this->imageSnakehead, NULL, SDL::ecran, &positionSnakeHead);
            }
            if (Map[i][j] == 'o') {
                this->positionSnakeCorp.x = j + pposX;
                this->positionSnakeCorp.y = i + posY;
                SDL_BlitSurface(this->imageSnakeCorp, NULL, SDL::ecran, &positionSnakeCorp);
            }
            if (Map[i][j] == 'P') {
                this->positionPomme.x = j + pposX;
                this->positionPomme.y = i + posY;
                SDL_BlitSurface(this->imagePomme, NULL, SDL::ecran, &positionPomme);
            }
            if (Map[i][j] == '.') {
                this->positionPacGomme.x = j + pposX;
                this->positionPacGomme.y = i + posY;
                SDL_BlitSurface(this->imagePacGomme, NULL, SDL::ecran, &positionPacGomme);
            }
            if (Map[i][j] == '@') {
                this->positionPacMan.x = j + pposX;
                this->positionPacMan.y = i + posY;
                SDL_BlitSurface(this->imagePacMan, NULL, SDL::ecran, &positionPacMan);
            }
            if (Map[i][j] == '&') {
                this->positionGhost1.x = j + pposX;
                this->positionGhost1.y = i + posY;
                SDL_BlitSurface(this->imageGhost1, NULL, SDL::ecran, &positionGhost1);
            }
            if (Map[i][j] == '"') {
                this->positionGhost2.x = j + pposX;
                this->positionGhost2.y = i + posY;
                SDL_BlitSurface(this->imageGhost2, NULL, SDL::ecran, &positionGhost2);
            }
            if (Map[i][j] == '=') {
                this->positionGhost3.x = j + pposX;
                this->positionGhost3.y = i + posY;
                SDL_BlitSurface(this->imageGhost3, NULL, SDL::ecran, &positionGhost3);
            }
            if (Map[i][j] == '-') {
                this->positionGhost4.x = j + pposX;
                this->positionGhost4.y = i + posY;
                SDL_BlitSurface(this->imageGhost4, NULL, SDL::ecran, &positionGhost4);
            }
            if (Map[i][j] == '+') {
                this->positionPacBoule.x = j + pposX;
                this->positionPacBoule.y = i + posY;
                SDL_BlitSurface(this->imagePacBoule, NULL, SDL::ecran, &positionPacBoule);
            }
        }
        pposX = 50;
    }
    SDL_Delay(80);
    return (Map);
}
void SDL_map::SDL_Map::SDL_gameOver()
{
    exit(0);
}

void SDL_map::SDL_Map::SDL_Win()
{
    exit(0);
}

SDL_map::SDL_Map::~SDL_Map()
{
}

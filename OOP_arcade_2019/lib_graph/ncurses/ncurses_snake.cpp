/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** ncurses_snake
*/

#include "../../include/ncurses_menu.hpp"
#include "../../include/ncurses_snake.hpp"
#include "../../include/core.hpp"
#include "../../include/GameSnake.hpp"
#include <ncurses.h>
#include <curses.h>

int snake_ncurses::map_ncurses(std::vector<std::string> Map) 
{
    int ret = 0;
    for (size_t i = 0; i < Map.size(); i++) {
        for (size_t j = 0; j < Map[i].length(); j++) {
            if (Map[i][j] == '*') {
                Map[i][j] = ' ';
            }
        }
         mvwprintw(this->snake_win,i + 1,2, Map[i].c_str());
    }
    wrefresh(this->snake_win);
    return (ret);
}

int snake_ncurses::game_loop(std::vector<std::string> Map) 
{
    int key = 0;
    int dir = 0;
    bool quit = false;
    char buffer[200];

    game::GameSnake snake;
    snake.score = 0;
    snake.Exit = 0;
    std::vector<std::string> MapSnake;
    snake.read_highscore();
    MapSnake = Map;
    initscr();
    this->snake_win = newwin(30, 80, 0, 0);
    box(this->snake_win, 0 ,0);
    noecho();
    keypad(this->snake_win, TRUE );
    curs_set( 0 );
    nodelay(this->snake_win,1);
    while (quit == false) {
        map_ncurses(MapSnake);
        usleep(80000);
        key = wgetch(this->snake_win);
        switch (key) {
            case KEY_UP:
                dir = 2;
                break;
            case KEY_LEFT:
                dir = 1;
                break;
            case KEY_RIGHT:
                dir = 3;
                break;
            case KEY_DOWN:
                dir = 4; 
                break;
            case 'q':
                quit = true;
                break;
            case 'p':
                dir = 5;
                this->lib = 1;
                this->game = 1;
                quit = true;
                break;
            case 'n':
                dir = 6;
                this->lib = 3;
                this->game = 1;
                quit = true;
                break;
            case 'w':
                dir = 9;
                this->lib = 2;
                this->game = 2;
                quit = true;
                break;
        }
        MapSnake = snake.beginSnake(MapSnake, dir);
        if (snake.Exit >= 1)
            quit = true;
        sprintf(buffer, "score: %d", snake.score);
        mvwprintw(this->snake_win,5, 50, buffer);
        sprintf(buffer, "highscore: %d", snake.highscore);
        mvwprintw(this->snake_win, 8, 50., buffer);
    }
    snake.write_highscore();
    delwin(this->snake_win);
    endwin();
    this->Exit = snake.Exit;
   return (dir);
}

void snake_ncurses::ncurses_game_over()
{
    menu_ncurses menu_ncurses;
    initscr();
    this->snake_game_over = newwin(30, 80, 0, 0);
    box(this->snake_game_over, 0 ,0);
    noecho();
    keypad(this->snake_game_over, TRUE );
    curs_set( 0 );
    mvwprintw(this->snake_game_over, 5,  15, "YOU LOSE");
    wgetch(this->snake_game_over);
    wclear(this->snake_game_over);
    wrefresh(this->snake_game_over);
    delwin(this->snake_game_over);
    endwin();
}

void snake_ncurses::ncurses_game_win()
{
    menu_ncurses menu_ncurses;
    initscr();
    this->snake_game_win = newwin(30, 80, 0, 0);
    box(this->snake_game_win, 0 ,0);
    noecho();
    keypad(this->snake_game_win, TRUE );
    curs_set( 0 );
    mvwprintw(this->snake_game_win, 5,  15, "YOU WIN");
    wgetch(this->snake_game_win);
    wclear(this->snake_game_win);
    wrefresh(this->snake_game_win);
    delwin(this->snake_game_win);
    endwin();
}
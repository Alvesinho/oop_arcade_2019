Arcade
An epitech project

Project realised in C++

Prerequisites
A unix environment
OpenGL (and glew)
SDL 2.0
SFML
Usage
launch with:

./arcade ./lib/lib_arcade_ncurse.so for Ncurse
./arcade ./lib/lib_arcade_sdl.so for SDL
./arcade ./lib/lib_arcade_sfml.so for SFML
